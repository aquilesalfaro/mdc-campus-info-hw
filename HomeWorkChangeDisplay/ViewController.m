//"
//  ViewController.m
//  HomeWorkChangeDisplay
//
//  Created by Aquiles Alfaro on 10/10/17.
//  Copyright © 2017 Aquiles Alfaro. All rights reserved.
//

#import "ViewController.h"
int itemNo = 0;
NSString *const campuses[] = { @"hialeah.jpeg", @"homestead.jpeg", @"kendall.jpeg", @"north.jpeg", @"south.jpeg", @"west.jpeg", @"wolfson.jpeg"};


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgDisplay;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
   self.imgDisplay.image = [UIImage imageNamed:campuses[itemNo]];
    [self setLabelText:itemNo];
    if (itemNo == 0)
        self.btnPrev.enabled = false;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Previous:(id)sender {
    if (itemNo != 0){
    itemNo -= 1;
        _btnNext.enabled = true;
    }
    if (itemNo == 0){
        _btnPrev.enabled = false;
    }
       self.imgDisplay.image = [UIImage imageNamed:campuses[itemNo]];
    [self setLabelText:itemNo];
}
- (IBAction)Next:(id)sender {
    if (itemNo != 6){
        itemNo += 1;
        _btnPrev.enabled = true;
    }
    if (itemNo == 6){
        _btnNext.enabled = false;
    }
       self.imgDisplay.image = [UIImage imageNamed:campuses[itemNo]];
    [self setLabelText:itemNo];
}
-(void)setLabelText:(int) item{
    if (item == 0){
        _lblDescription.text = @"Miami Dade Hialeah Campus\n"
        "North Miami, Florida";
        
    }else if (item == 1){
        _lblDescription.text = @"Miami Dade Homestead Campus\n"
        "Homestead, Florida";
    }else if (item == 2){
        _lblDescription.text = @"Miami Dade Kendal Campus\n"
        "Kendall, Florida";
    }else if (item == 3){
        _lblDescription.text = @"Miami Dade North Campus\n"
        "North Miami, Florida";
    }else if (item == 4){
        _lblDescription.text = @"Miami Dade South Campus\n"
        "South Miami, Florida";
    }else if (item == 5){
        _lblDescription.text = @"Miami Dade West Campus\n"
        "West Miami, Florida";
    }else if (item == 6){
        _lblDescription.text = @"Miami Dade Wolfson Campus\n"
        "Downtown Miami, Florida";
    }
    
}


@end
